﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Week1.Zoology;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            // Decalring lists of each animal object
            List<Lion> lionlist = new List<Lion>();
            List<Elefant> elefantList = new List<Elefant>();
            List<Dog> dogList = new List<Dog>();
            List<Zoology> list3 = new List<Zoology>();


            //Creating dog objects and adding them to the corresponding list
            Dog dog = new Dog();
            Dog dog2 = new Dog("Mammal", "Omnivores", "Dobberman", 4, 15);
            Dog dog3 = new Dog("Mammal", "Omnivores", "San Bernadiner", 2, 28);
            Dog dog4 = new Dog("Mammal", "Omnivores", "Great Dane", 5, 55);
            Dog dog5 = new Dog("Mammal", "Omnivores", "Mastiff", 4, 100);
            dogList.Add(dog);
            dogList.Add(dog2);
            dogList.Add(dog3);
            dogList.Add(dog4);
            dogList.Add(dog5);


            // Creating lion objects and adding them to the corresponding list
            Lion lion = new Lion();
            Lion lion2 = new Lion("Mammal", "Carnivore", "African Lion", 150, 35);
            Lion lion3 = new Lion("Mammal", "Carnivore", "Congo Lion", 160, 45);
            Lion lion4 = new Lion("Mammal", "Carnivore", "Asiatic Lion", 130, 38);
            Lion lion5 = new Lion("Mammal", "Carnivore", "Barbary Lion", 110, 49);
            lionlist.Add(lion);
            lionlist.Add(lion2);
            lionlist.Add(lion3);
            lionlist.Add(lion4);
            lionlist.Add(lion5);


            // Creating elefant objects and adding them to the corresponding list
            Elefant elefant = new Elefant();
            Elefant elefant2 = new Elefant("Mammal", "Herbavore", "African bush Elefant", 4, 4000);
            Elefant elefant3 = new Elefant("Mammal", "Herbavore", "Palaeoloxodon naumanni", 5, 4700);
            Elefant elefant4 = new Elefant("Mammal", "Herbavore", "Asian Elefant", 3.5, 3200);
            Elefant elefant5 = new Elefant("Mammal", "Herbavore", "Sri Lanka Elefant", 4.2, 3800);
            elefantList.Add(elefant);
            elefantList.Add(elefant2);
            elefantList.Add(elefant3);
            elefantList.Add(elefant4);
            elefantList.Add(elefant5);


            /*
             Creating the Linq querries for each animal type
             */
            IEnumerable<Dog> dogResult = from animal in dogList
                                         where animal.Weight > 40
                                         select animal;

            IEnumerable<Lion> lionResult = from animal in lionlist
                                           where animal.Speed > 30
                                           select animal;

            IEnumerable<Elefant> elefantResult = from animal in elefantList
                                                 where animal.Height > 4
                                                 select animal;



            /*
             Looping through each of the linq querried lists and displaying the results. 
             */
            foreach (var animal in dogResult)
            {
                Console.WriteLine("This dog is heavier than 40kg " + animal.Type);
            } 
            
            foreach (var animal in lionResult)
            {
                Console.WriteLine($"This {animal.Type} is faster than 30 km/h with a speed of: " + animal.Speed+" km/h");
            } 
            
            
            foreach (var animal in elefantResult)
            {
                Console.WriteLine($"This {animal.Type} is higher than 4 meters with a height of: {animal.Height} meters.");
            }
        }
    }
}
