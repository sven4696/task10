﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1.Zoology
{
    public abstract class Zoology
    {
        public string Family { get; set; }
        public string Diet { get; set; }

        public string Type { get; set; }

        // Default Constructor
        public Zoology()
        {

        }

        // Overrloaded Constructor
        public Zoology(string Family, string Diet, string Type)
        {
            this.Family = Family;
            this.Diet = Diet;
            this.Type = Type;
        }

        // Abstract void method which must be implemented in sub classes
        public abstract void Nutrition();

        // Sound method which descries the sound of the animal 
        public virtual void Sound()
        {
            Console.WriteLine("The animal makes a rawr sound!");
        }
    }

    
}
