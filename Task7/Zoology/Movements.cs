﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Week1.Zoology
{
    class Movements
    {

        /*
         Interface which implements the movements of animals
         */
        public interface IClimber
        {
            void Climb();
            
        }

        public interface ISwimmer
        {
            void Swim();
       
        }

        public interface IRun
        {
            void Run();
            
        }
    }
}
