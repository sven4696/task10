﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1.Zoology
{
    class Elefant : Zoology, Movements.ISwimmer, Movements.IRun
    {
        public string Type { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }

        //Default constructor
        public Elefant() : base("Mammal", "Vegetarian", "Elefant")
        {
            Type = "African Elefant";
            Height = 4.5;
            Weight = 4000;
        }

        // Overloaded constructor
        public Elefant(string Family, string Diet, string Type, double Height, double Weight) : base(Family, Diet, "Elefant")
        {
            this.Type = Type;
            this.Height = Height;
            this.Weight = Height;
        }

        // void method which provides a descrption of the elefant object created
        public void Description()
        {
            Console.WriteLine($"the {this.Type} is a {this.Family} and has a {this.Diet} diet standing {this.Height} meters tall and weighs a whooping {this.Weight} kg.");
        }

        // Override void method which describes the sound of the animal 
        public override void Sound()
        {
            Console.WriteLine($"The {this.Type} makes the sound brrr!");
        }

        // Override void method which describes the diet of the animal
        public override void Nutrition()
        {
            Console.WriteLine($"The {this.Type} is a {this.Diet} so he only eats plants and greens.");
        }

        public void Swim()
        {
            Console.WriteLine($"The {this.Type} is able to Swimm");
        }

        public void Run()
        {
            Console.WriteLine($"The {this.Type} is able to run faster than a human!");
        }


    }
}
