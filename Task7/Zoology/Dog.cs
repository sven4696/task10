﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1.Zoology
{
    public class Dog : Zoology, Movements.ISwimmer, Movements.IRun
    {
        public string Type { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }

        //Default constructor
        public Dog() : base("Mammal", "Omnivores", "Dog")
        {
            Type = "Staffi";
            Age = 1;
            Weight = 100;
        }

        // Overloaded constructor
        public Dog (string Family, string Diet, string Type, int Age, double Weight) : base(Family, Diet, "Dog")
        {
            this.Type = Type;
            this.Age = Age;
            this.Weight = Weight;
        }

        // void method which provides a descrption of the dog object created
        public void Description()
        {
            Console.WriteLine($"The {this.Type} comes from the {this.Family} family and has a {this.Diet} diet, he is {this.Age} years old and weighs {this.Weight} kg.");
        }

        // Override void method which describes the sound of the animal 
        public override void Sound()
        {
            Console.WriteLine($"The {this.Type} makes the sound wuff! ");
        }

        // Override void method which describes the diet of the animal
        public override void Nutrition()
        {
            Console.WriteLine($"The {this.Type} is a {this.Diet} and therefore, his meals consists of both plants and meats.");
        }

        public void Swim()
        {
            Console.WriteLine($"The {this.Type} is able to Swimm");
        }

        public void Run()
        {
            Console.WriteLine($"The {this.Type} is able to run faster than a human!");
        }

    }
}
