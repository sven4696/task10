﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1.Zoology
{
    class Lion : Zoology, Movements.IClimber, Movements.ISwimmer, Movements.IRun

    {

        public string Type { get; set; }
        public double Weight { get; set; }
        public double Speed { get; set; }

        //Default constructor
        public Lion () : base("Mammal", "Carnivore", "Lion")
        {
            Type = "African Lion";
            Weight = 180;
            Speed = 80;
        }

        // Overloaded constructor
        public Lion(string Family, string Diet, string Type, double Weight, double Speed) : base(Family, Diet, "Lion")
        {
            this.Type = Type;
            this.Weight = Weight;
            this.Speed = Speed;
        }


        // void method which provides a descrption of the lion object created
        public void Description()
        {
            Console.WriteLine($"the {this.Type} is a {this.Family} and has a {this.Diet} diet, he weighs a whooping {this.Weight} kg and can run up to {this.Speed} km/h for short time intervalls.");
        }

        // Override void method which describes the diet of the animal
        public override void Nutrition()
        {
            Console.WriteLine($"The {this.Type} has a {this.Diet} and therefore, only eats meat.");
        }

        public void Climb()
        {
            Console.WriteLine($"The {this.Type} is able to climb trees");
        }

        public void Swim()
        {
            Console.WriteLine($"The {this.Type} is able to Swimm");
        }

        public void Run()
        {
            Console.WriteLine($"The {this.Type} is able to run faster than a human!");
        }
    }
}
